'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.get('/dashboard', function (req, res) {
  var user = req.user;

  if (user) {
    res.render('profile', { user: user });
  } else {
    res.render('index');
  }
});

router.get('/login', _passport2.default.authenticate('oauth2'));

router.get('/', _passport2.default.authenticate('oauth2', {
  failureRedirect: '/index',
  successRedirect: '/dashboard'
}));

exports.default = router;
//# sourceMappingURL=index.js.map