'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setupDefaultRoutes = exports.setupMiddleware = undefined;

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _expressSession = require('express-session');

var _expressSession2 = _interopRequireDefault(_expressSession);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setupMiddleware = exports.setupMiddleware = function setupMiddleware(app) {
  // view engine setup
  app.set('views', _path2.default.join(__dirname, '../src/views'));
  app.set('view engine', 'pug');

  // uncomment after placing your favicon in /public
  //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
  app.use((0, _morgan2.default)('dev'));
  app.use(_bodyParser2.default.json());
  app.use(_bodyParser2.default.urlencoded({ extended: false }));
  app.use((0, _cookieParser2.default)());
  (0, _auth2.default)();
  app.use((0, _expressSession2.default)({
    secret: 'somedummysecret',
    maxAge: null,
    resave: false,
    saveUninitialized: true
  }));
  app.use(_passport2.default.initialize());
  app.use(_passport2.default.session());
  app.use('/public', _express2.default.static(_path2.default.join(__dirname, '../public')));
};

var setupDefaultRoutes = exports.setupDefaultRoutes = function setupDefaultRoutes(app) {
  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handler
  app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
};
//# sourceMappingURL=middleware-setup.js.map