'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _passportOauth = require('passport-oauth2');

var _passportOauth2 = _interopRequireDefault(_passportOauth);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _rsvp = require('rsvp');

var _rsvp2 = _interopRequireDefault(_rsvp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var request = _rsvp2.default.denodeify(_request2.default);

var url = 'https://staging-auth.wallstreetdocs.com/oauth/userinfo';

var authSetup = function authSetup() {

  _passport2.default.serializeUser(function (user, done) {
    done(null, user);
  });

  _passport2.default.deserializeUser(function (user, done) {
    done(null, user);
  });

  _passport2.default.use(new _passportOauth2.default({
    authorizationURL: 'https://staging-auth.wallstreetdocs.com/oauth/authorize',
    tokenURL: 'https://staging-auth.wallstreetdocs.com/oauth/token',
    clientID: 'coding_test',
    clientSecret: 'bwZm5XC6HTlr3fcdzRnD',
    callbackURL: "http://localhost:3000"
  }, function () {
    var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(accessToken, refreshToken, profile, cb) {
      var response;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return request({
                url: url,
                headers: {
                  "Authorization": 'Bearer ' + accessToken,
                  "Cache-Control": 'no-cache'
                }
              });

            case 2:
              response = _context.sent;
              return _context.abrupt('return', cb(null, JSON.parse(response.body)));

            case 4:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    return function (_x, _x2, _x3, _x4) {
      return _ref.apply(this, arguments);
    };
  }()));
};

exports.default = authSetup;
//# sourceMappingURL=auth.js.map