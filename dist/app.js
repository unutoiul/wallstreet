'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

require('babel-polyfill');

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _middlewareSetup = require('./middleware-setup');

var _index = require('./routes/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var port = process.env.PORT || '3000';

var app = (0, _express2.default)();

(0, _middlewareSetup.setupMiddleware)(app);

app.use('/', _index2.default);

(0, _middlewareSetup.setupDefaultRoutes)(app);

app.set('port', port);

var appListen = function appListen() {
  app.listen(port, function () {
    console.log('Listening on port ' + port);
  });
};

exports.default = appListen;
//# sourceMappingURL=app.js.map